// This program is a driver that tests a function comparing the 
// contents of two int arrays. 
#include <iostream>
#include <vector>
using namespace std;

const int NUM_DIGITS = 7; // Number of digits in a PIN

bool testPIN(vector <int>, vector <int>, const int); // Function Prototype 

int main()
{
	vector <int> pin1 = { 2, 4, 1, 8, 7, 9, 0 };
	vector <int> pin2 = { 2, 4, 6, 8, 7, 9, 0 };
	vector <int> pin3 = { 1, 2, 3, 4, 5, 6, 7 };
	char again = 'y';

	/*  ORIGINAL
	int pin1[NUM_DIGITS] = { 2, 4, 1, 8, 7, 9, 0 }; // Base set of values.
	int pin2[NUM_DIGITS] = { 2, 4, 6, 8, 7, 9, 0 }; // Only 1 element is
	// different from pin1.
	int pin3[NUM_DIGITS] = { 1, 2, 3, 4, 5, 6, 7 }; // All elements are
	// different from pin1.
	*/

	while (again == 'y' || again == 'Y'){
		cout << "Pin 1 values: ";
		for (int i = 0; i < pin1.size(); i++)
			cout << pin1.at(i) << " ";
		
		cout << "\nPin 2 values: ";
		for (int j = 0; j < pin2.size(); j++)
			cout << pin2.at(j) << " ";
		
		cout << "\nPin 3 values: ";
		for (int k = 0; k < pin3.size(); k++)
			cout << pin3.at(k) << " ";
		cout << endl << endl;

		if (testPIN(pin1, pin2, NUM_DIGITS))
			cout << "ERROR: pin1 and pin2 report to be the same.\n";
		else
			cout << "SUCCESS: pin1 and pin2 are different.\n";

		if (testPIN(pin1, pin3, NUM_DIGITS))
			cout << "ERROR: pin1 and pin3 report to be the same.\n";
		else
			cout << "SUCCESS: pin1 and pin3 are different.\n";

		if (testPIN(pin1, pin1, NUM_DIGITS))
			cout << "SUCCESS: pin1 and pin1 report to be the same.\n";
		else
			cout << "ERROR: pin1 and pin1 report to be different.\n";

		cout << "\n\t\t\tEnter y or Y to repeat.\n";
		cin >> again;
	}
}

//****************************************************************** 
// The following function accepts two int arrays. The arrays are * 
// compared. If they contain the same values, true is returned. * 
// If they contain different values, false is returned. * 
//****************************************************************** 
bool testPIN(vector <int> custPIN, vector <int> databasePIN, const int size)
{
	for (int index = 0; index < size; index++)
	{
		if (custPIN[index] != databasePIN[index])
			return false; // We've found two different values. 
	}
	return true; // If we make it this far, the values are the same. 
}