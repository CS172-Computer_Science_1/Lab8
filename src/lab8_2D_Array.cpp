#include <iostream>
#include <iomanip>
using namespace std;

const int ROWS = 3;
const int COLS = 4;

void getTotal(int[ROWS][COLS], int &);
void getAverage(int[ROWS][COLS], double&);
void getRowTotal(int[ROWS][COLS], int, int&);
void getColumnTotal(int[ROWS][COLS], int, int&);
void getHighestInRow(int[ROWS][COLS], int, int&);
void getLowestInRow(int[ROWS][COLS], int, int&);
void tblImg(int[ROWS][COLS]); //values table
void err(); //fix cin errors

int main(){
	char again = 'y';
	while (again == 'y' || again == 'Y'){
		int arrayx[ROWS][COLS] = { { 31, 41, 11, 91 }, { 62, 32, 72, 12 }, { 93, 73, 33, 23 } },
			ttl = 0, trow = 0, tcol = 0, hrow = 0, lrow = 0, usrRow = 0, usrCol = 0;
		double avg = 0;

		tblImg(arrayx); //display table to user

		cout << "Enter a Row (1-3): ";
		cin >> usrRow;
		while (cin.fail() || usrRow < 1 || usrRow > 3){ //fix letter and negative
			err();
			cout << "Enter a Row (1-3): ";
			cin >> usrRow;
		}

		cout << "Enter a Column (1-4): ";
		cin >> usrCol;
		while (cin.fail() || usrCol < 1 || usrCol > 4){ //fix letter and negative
			err();
			cout << "Enter a Column (1-4): ";
			cin >> usrCol;
		}

		getTotal(arrayx, ttl);
		getAverage(arrayx, avg);
		getRowTotal(arrayx, usrRow , trow);
		getColumnTotal(arrayx, usrCol, tcol);
		getHighestInRow(arrayx, usrRow, hrow);
		getLowestInRow(arrayx, usrRow, lrow);
		cout << "Total: " << ttl << endl
			<< "Average: " << setprecision(2) << fixed << avg << flush << endl
			<< "Row (" << usrRow << ") total: " << trow << endl
			<< "Column (" << usrCol << ") total: " << tcol << endl
			<< "Highest number in row (" << usrRow << ") is: " << hrow << endl
			<< "Lowest number in row (" << usrRow << ") is:" << lrow << endl;

		cout << "\n\t\t\tEnter y or Y to repeat: ";
		cin >> again;
	}
}
void getTotal(int ttlArray[ROWS][COLS], int& ttlOut){
	for (int ir = 0; ir < ROWS; ir++){
		for (int ic = 0; ic < COLS; ic++)
			ttlOut += ttlArray[ir][ic];
	}
}
void getAverage(int avgArray[ROWS][COLS], double& avgOut){
	for (int ir = 0; ir < ROWS; ir++){
		for (int ic = 0; ic < COLS; ic++)
			avgOut += avgArray[ir][ic]; //sum
	}
	avgOut /= (ROWS*COLS); //avg
}
void getRowTotal(int rowArray[ROWS][COLS], int row, int& ttlRow){
	row -= 1; //usr to real
	for (int ic = 0; ic < COLS; ic++)
		ttlRow += rowArray[row][ic];
}
void getColumnTotal(int colArray[ROWS][COLS], int col, int& ttlCol){
	col -= 1; //usr to real
	for (int ir = 0; ir < ROWS; ir++)
		ttlCol += colArray[ir][col];
}
void getHighestInRow(int rowHigh[ROWS][COLS], int row, int& rowHighOut){
	row -= 1; //usr to real
	for (int ic = 0; ic < COLS; ic++){
		if (rowHigh[row][ic] >= rowHighOut) //value greater than output, replace
			rowHighOut = rowHigh[row][ic];
	}
}
void getLowestInRow(int rowLow[ROWS][COLS], int row, int& rowLowOut){
	row -= 1; //usr to real
	rowLowOut = rowLow[row][0]; //initialize
	for (int ic = 0; ic < COLS; ic++){
		if (rowLow[row][ic] <= rowLowOut) //output greater than value, replace
			rowLowOut = rowLow[row][ic];
	}
}
void tblImg(int xRay[ROWS][COLS]){
	cout << "\n       ||C1||C2||C3||C4||\n"
		<< "       ------------------\n";
	for (int ir = 0; ir < ROWS; ir++){
		cout << "(Row " << ir + 1 << ")||";
		for (int ic = 0; ic < COLS; ic++)
			cout << setw(2) << setfill('0') << xRay[ir][ic] << "||";
		cout << endl;
	}
	cout << endl;
}
void err(){
	cout << "\t\t/!\\ ERROR /!\\\n";	
	cin.clear();
	fflush(stdin);
}